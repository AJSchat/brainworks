/*
* syn.h
* Copyright (C) 2001-2002 Raven Software
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3.0 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library.
* If not, see <http://www.gnu.org/licenses/>.
*/

//===========================================================================
//
// Name:			syn.h
// Function:		synonyms
// Programmer:		Mr Elusive
// Last update:		-
// Tab Size:		4 (real tabs)
// Notes:			-
//===========================================================================

#define CONTEXT_ALL						0xFFFFFFFF
#define CONTEXT_NORMAL					1
#define CONTEXT_NEARBYITEM				2
#define CONTEXT_CTFREDTEAM				4
#define CONTEXT_CTFBLUETEAM				8
#define CONTEXT_REPLY					16

#define CONTEXT_NAMES 1024
