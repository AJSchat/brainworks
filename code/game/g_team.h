/*
* g_team.h -- game module information visible to server
* Copyright (C) 2001-2002 Raven Software
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3.0 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library.
* If not, see <http://www.gnu.org/licenses/>.
*/

int				OtherTeam				( team_t team );
const char*		TeamName				( team_t team );
const char*		TeamSkins				( team_t team );
const char*		OtherTeamName			( team_t team );
const char*		TeamColorString			( team_t team );
void			G_AddTeamScore			( team_t team, int score );

gentity_t*		Team_GetLocation		( gentity_t *ent, qboolean pvs );
qboolean		Team_GetLocationMsg		( gentity_t *ent, char *loc, int loclen );


