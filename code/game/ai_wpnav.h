/*
* ai_wpnav.h
* Copyright (C) 2001-2002 Raven Software
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3.0 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library.
* If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _AI_WPNAV_H
#define _AI_WPNAV_H

// Function declarations.
int wpNav_AcceptBotCommand	(char *cmd, gentity_t *pl);
int	wpNav_LoadPathData		(const char *filename);
int wpNav_SavePathData		(const char *filename);

#endif // _AI_WPNAV_H
