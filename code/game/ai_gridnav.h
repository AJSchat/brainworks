/*
* ai_gridnav.h
* Copyright (C) 2001-2002 Raven Software
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3.0 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library.
* If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _AI_GRIDNAV_H
#define _AI_GRIDNAV_H

#define MAX_NODE_ARRAY_SIZE 2048
#define MAX_NODE_NEIGHBORS	16
#define NODE_BOUND_SIZE		10		// How large distance is between the center and the boundary of the in-game "X".

// The max store string is calculated to define the max boundary of a written node to the path data file.
// It is determined with the following data:
//  - 13 bytes for each float.
//  - 4 bytes for the index integer.
//  - 2 bytes for the flags integer.
//  - MAX_NODE_NEIGHBORS * 4 bytes for neighborIds integer array.
//  - Written arrays * 4 bytes for opening brackets + closing brackets + spaces.
//  - And 1 byte for every member -1 written to the file (space that follows).
#define MAX_GNAV_STORESTRING 149

#define MAX_GNAV_FETCHSTRING 13		// Should match the size of the biggest value (in bytes) in the store string.
#define MAX_GNAV_FILESIZE	305152	// MAX_NODE_ARRAY_SIZE * MAX_GNAV_STORESTRING.

typedef struct gnavobject_s
{
	vec3_t		origin; 						// The origin of the node.
	qboolean	inuse;							// True if this node is in use.
	int			index;							// Index in the node array.
	float		weight;							// The weight translates to how important the node is.
	float		disttonext[MAX_NODE_NEIGHBORS];	// The distance from this node to its neighbors.
	int			flags;							// The associated flags to this node.

	int			numNeighbors;					// Stores how many connected neighbors a path has.
	int			neighborIds[MAX_NODE_NEIGHBORS];// Stores how many paths (and thus nodes) are connected to this node.
} gnavobject_t;

// Function declarations.
int			gridNav_LoadPathData	(const char *fileName);
int			gridNav_SavePathData	(gentity_t *ent, const char *fileName);
int			gridNav_AcceptBotCommand(char *cmd, gentity_t *pl);

int			gridNav_FindEmptyNode	(void);
void		gridNav_CreateNewNode	(vec3_t origin, int flags);
void 		gridNav_SelectNode		(gentity_t *pl);
qboolean 	gridNav_ConnectNode		(gentity_t *pl, int start, int end);
void		gridNav_DisconnectNode	(gentity_t *pl);
void		gridNav_DeleteNode		(gentity_t *pl, int index);

void		gridNav_DrawLine		(vec3_t start, vec3_t end, int i);
void		gridNav_DrawNode		(vec3_t centerOrigin, int i);

#endif // _AI_GRIDNAV_H
