/*
* ai_gridnav.c -- Main routines regarding the gridnav system.
* Copyright (C) 2014 Ane-Jouke Schat
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3.0 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library.
* If not, see <http://www.gnu.org/licenses/>.
*/

#include "g_local.h"
#include "q_shared.h"
#include "botlib.h"

#include "ai_main.h"
#include "ai_gridnav.h"

float			gnavRenderTime = 0;
int				gnavRenderedFrame = 0;
int				numNodes = 0;
int				gnavLastPrintedIndex = -1;
int				selectedNode = -1;

qboolean		createTrail = qfalse;
int				gnavLastVisitedNode = -1;	// For creating connections between nodes.

gnavobject_t	*gnavNodes[MAX_NODE_ARRAY_SIZE];	// This array holds all nodes in the level.

/*
================================
gridNav_LoadPathData

This function loads all gridnav 
data from the *.gnav file.
================================
*/

int	gridNav_LoadPathData(const char *fileName)
{
	fileHandle_t	 f;
	char			*fileString;
	char			*currentVar;
	int		 		 i, n, i_cv;
	int				len, nodeNum;
	
	Com_Printf("Loading Gridnav data from: botroutes/%s.gnav\n", fileName);
	len = trap_FS_FOpenFile(va("botroutes/%s.gnav", fileName), &f, FS_READ);
	
	if (!f){
		Com_Printf("Error: file not found\n");
		return 2;
	}
	
	if (len >= MAX_GNAV_FILESIZE){
		Com_Printf(S_COLOR_RED "Error: Route file exceeds maximum length.\n");
		return 0;
	}
	
	#ifdef Q3_VM
	fileString = (char *)B_TempAlloc(MAX_GNAV_FILESIZE);
	currentVar = (char *)B_TempAlloc(MAX_GNAV_FETCHSTRING);
	#else
	fileString = malloc(sizeof(char) * MAX_GNAV_FILESIZE);
	currentVar = malloc(sizeof(char) * MAX_GNAV_FETCHSTRING);
	#endif // Q3_VM
	
	// Read the file.
	trap_FS_Read(fileString, len, f);
	
	i = 0;
	while (i < len)
	{
		// Start parsing info from the file.
		// Have some sort of sanity checks while fetching.
		i_cv = 0;
		
		// Fetch index.
		while (fileString[i] != ' ')
		{
			if(i_cv > MAX_GNAV_FETCHSTRING){
				Com_Printf(S_COLOR_RED "Error: fetching index in route file - value too long.\n");
				return 0;
			}
			
			currentVar[i_cv] = fileString[i];
			i_cv++;
			i++;
		}
		currentVar[i_cv] = '\0';
		nodeNum = atoi(currentVar);
		
		if(nodeNum < 0 || nodeNum > MAX_NODE_ARRAY_SIZE){
			Com_Printf(S_COLOR_RED "Error: node number %i out of range.\n", nodeNum);
		}
		
		// Check if it's been initialized yet.
		if(!gnavNodes[nodeNum]){
			gnavNodes[nodeNum] = (gnavobject_t *)B_Alloc(sizeof(gnavobject_t));
		}
		gnavNodes[nodeNum]->index = atoi(currentVar); // Set index.
		
		i_cv = 0;
		i++;
		
		// Fetch flags.
		while (fileString[i] != ' ')
		{
			if(i_cv > MAX_GNAV_FETCHSTRING){
				Com_Printf(S_COLOR_RED "Error: fetching flags in route file - value too long.\n");
				return 0;
			}
			
			currentVar[i_cv] = fileString[i];
			i_cv++;
			i++;
		}
		currentVar[i_cv] = '\0';
		gnavNodes[nodeNum]->flags = atoi(currentVar); // Set flags.
		
		i_cv = 0;
		i++;
		
		// Fetch weight.
		while (fileString[i] != ' ')
		{
			if(i_cv > MAX_GNAV_FETCHSTRING){
				Com_Printf(S_COLOR_RED "Error: fetching weight in route file - value too long.\n");
				return 0;
			}
			
			currentVar[i_cv] = fileString[i];
			i_cv++;
			i++;
		}
		currentVar[i_cv] = '\0';
		gnavNodes[nodeNum]->weight = atof(currentVar); // Set weight.
		
		i_cv = 0;
		i++;
		
		// Fetch origin.
		while (fileString[i] != ' ')
		{
			if(i_cv > MAX_GNAV_FETCHSTRING){
				Com_Printf(S_COLOR_RED "Error: fetching origin in route file - value too long.\n");
				return 0;
			}
			
			currentVar[i_cv] = fileString[i];
			i_cv++;
			i++;
		}
		currentVar[i_cv] = '\0';
		gnavNodes[nodeNum]->origin[0] = atof(currentVar); // Set origin[0].
		
		i_cv = 0;
		i++;
		
		while (fileString[i] != ' ')
		{
			if(i_cv > MAX_GNAV_FETCHSTRING){
				Com_Printf(S_COLOR_RED "Error: fetching origin in route file - value too long.\n");
				return 0;
			}
			
			currentVar[i_cv] = fileString[i];
			i_cv++;
			i++;
		}
		currentVar[i_cv] = '\0';
		gnavNodes[nodeNum]->origin[1] = atof(currentVar); // Set origin[1].
		
		i_cv = 0;
		i++;
		
		while (fileString[i] != ' ')
		{
			if(i_cv > MAX_GNAV_FETCHSTRING){
				Com_Printf(S_COLOR_RED "Error: fetching origin in route file - value too long.\n");
				return 0;
			}
			
			currentVar[i_cv] = fileString[i];
			i_cv++;
			i++;
		}
		currentVar[i_cv] = '\0';
		gnavNodes[nodeNum]->origin[2] = atof(currentVar); // Set origin[2].
		
		i_cv = 0;
		i++;
		
		if(fileString[i] != '{'){
			Com_Printf(S_COLOR_RED "Error: format error in route file (found '%c' while expecting '{').\n", fileString[i]);
			return 0;
		}
		
		i++;
		n = 0;
		// Fetch neighbor IDs (if present).
		while(fileString[i] != '}'){
			while (fileString[i] != ' ')
			{
				if(i_cv > MAX_GNAV_FETCHSTRING){
					Com_Printf(S_COLOR_RED "Error: fetching neighbor ID in route file - value too long.\n");
					return 0;
				}
				
				currentVar[i_cv] = fileString[i];
				i_cv++;
				i++;
			}
			
			if(i_cv){
				currentVar[i_cv] = '\0';
				gnavNodes[nodeNum]->neighborIds[n] = atoi(currentVar); // Set neighbor Ids.
			
				i_cv = 0;
				n++;
			}
			
			i++;
		}
		
		gnavNodes[nodeNum]->numNeighbors = n;
		gnavNodes[nodeNum]->inuse = qtrue;
		
		i += 2;
		numNodes++;
	}
	
	#ifdef Q3_VM
	B_TempFree(MAX_NAV_FILESIZE); //fileString
	B_TempFree(MAX_GNAV_FETCHSTRING); //currentVar
	#else
	free(fileString);
	free(currentVar);
	#endif // Q3_VM

	trap_FS_FCloseFile(f);
	
	Com_Printf("Successfully parsed route data.\n");
	return 1;
}

/*
================================
gridNav_SavePathData

This function saves all gridnav 
data to the *.gnav file.
================================
*/

int gridNav_SavePathData(gentity_t *ent, const char *fileName)
{
	fileHandle_t f;
	char *fileString;
	char *storeString;
	int i, count, n;

	if (!numNodes)
	{
		trap_SendServerCommand(ent-g_entities, "print \""
			BRAINWORKS_MSG "Error: There is no path in the level (yet)!\n\"");
		
		return 0;
	}
	
	trap_FS_FOpenFile(va("botroutes/%s.gnav", fileName), &f, FS_WRITE);
	
	if (!f)
	{
		trap_SendServerCommand(ent-g_entities, "print \""
			BRAINWORKS_MSG "Error: Could not open file to write path data!\n\"");
		
		return 0;
	}
	
	#ifdef Q3_VM
	fileString = (char *)B_TempAlloc(MAX_GNAV_FILESIZE);
	storeString = (char *)B_TempAlloc(MAX_GNAV_STORESTRING);
	#else
	fileString = malloc(sizeof(char) * MAX_GNAV_FILESIZE);
	memset(fileString, 0, sizeof(fileString));
	storeString = malloc(sizeof(char) * MAX_GNAV_STORESTRING);
	#endif // Q3_VM
	
	count = 0; // Counter of nodes that we've processed.
	// Loop through all nodes and append them to the storeString and ultimately fileString.
	for(i = 0; i < MAX_NODE_ARRAY_SIZE; i++){
		if(!gnavNodes[i] || !gnavNodes[i]->inuse){
			continue;
		}
		
		Com_sprintf(storeString, MAX_GNAV_STORESTRING, "%i %i %f %f %f %f { ", gnavNodes[i]->index, gnavNodes[i]->flags, gnavNodes[i]->weight, gnavNodes[i]->origin[0], gnavNodes[i]->origin[1], gnavNodes[i]->origin[2]);
		// Loop through all neighbors.
		n = 0;
		while(n < gnavNodes[i]->numNeighbors){
			Com_sprintf(storeString, MAX_GNAV_STORESTRING, "%s%i ", storeString, gnavNodes[i]->neighborIds[n]);
			n++;
		}
		
		// Append the final bracket and the newline and append it to the fileString.
		Com_sprintf(fileString, MAX_GNAV_FILESIZE, "%s%s}\n", fileString, storeString);
		
		count++;
		if(count == numNodes){
			break;
		}
	}
	
	trap_FS_Write(fileString, strlen(fileString), f);

	#ifdef Q3_VM
	B_TempFree(MAX_GNAV_FILESIZE); //fileString
	B_TempFree(MAX_GNAV_STORESTRING); //storeString
	#else
	free(fileString);
	free(storeString);
	#endif // Q3_VM

	trap_FS_FCloseFile(f);

	Com_Printf("Path data has been saved and updated. You may need to restart the level for some things to be properly calculated.\n");

	return 1;
}

/*
================================
gridNav_AcceptBotCommand

Handles all commands to the
gridnav system.
================================
*/

int gridNav_AcceptBotCommand(char *cmd, gentity_t *pl)
{
	char *OptionalSArgument;
	
	if (Q_stricmp (cmd, "bot_gnav_cmdlist") == 0) //lists all the bot waypoint commands.
	{
		trap_SendServerCommand(pl-g_entities, "print \""
			S_COLOR_MAGENTA "\nGridnav " S_COLOR_WHITE "command list\n"
			"-------------------------------------------------------\n\n"
			S_COLOR_MAGENTA "bot_gnav_save" S_COLOR_WHITE "       - Saves path data to file\n"
			S_COLOR_MAGENTA "bot_gnav_add" S_COLOR_WHITE "        - Add a node on your current origin\n"
			S_COLOR_MAGENTA "bot_gnav_select" S_COLOR_WHITE "     - Select a node (when near one)\n"
			S_COLOR_MAGENTA "bot_gnav_connect" S_COLOR_WHITE "    - Connects a node near you to the selected node\n"
			S_COLOR_MAGENTA "bot_gnav_disconnect" S_COLOR_WHITE " - Disconnects a node near you from the selected node\n"
			S_COLOR_MAGENTA "bot_gnav_delete" S_COLOR_WHITE "     - Deletes a node specified with an argument or near you\n"
			S_COLOR_MAGENTA "bot_gnav_trail" S_COLOR_WHITE "      - Auto connects each near node to the previous one\n"
			"\nUse ^6[Page Up] ^7and ^6[Page Down] ^7keys to scroll\n\n\"");
		
		return 1;
	}
	
	// Saves path data to the file.
	if (Q_stricmp (cmd, "bot_gnav_save") == 0 || Q_stricmp (cmd, "bot_gnav_savepath") == 0)
	{
		gDeactivated = 0; // FIXME
		gridNav_SavePathData(pl, mapname.string);
	}
	
	// Adds a node on the current origin.
	if (Q_stricmp (cmd, "bot_gnav_add") == 0 || Q_stricmp (cmd, "bot_gnav_addnode") == 0)
	{
		gDeactivated = 1; // FIXME
		gridNav_CreateNewNode(pl->client->ps.origin, 0);
		
		return 1;
	}
	
	// Selects the node based on where the player currently is.
	if(Q_stricmp (cmd, "bot_gnav_select") == 0 || Q_stricmp (cmd, "bot_gnav_selectnode") == 0)
	{
		gridNav_SelectNode(pl);
		
		return 1;
	}
	
	// Connects a previously selected node to the current node (where the player is at the moment).
	if(Q_stricmp (cmd, "bot_gnav_connect") == 0 || Q_stricmp (cmd, "bot_gnav_connectnode") == 0)
	{
		gridNav_ConnectNode(pl, -1, -1);
		
		return 1;
	}
	
	// Disconnects a previously selected node from the current node (where the player is at the moment).
	if(Q_stricmp (cmd, "bot_gnav_disconnect") == 0 || Q_stricmp (cmd, "bot_gnav_disconnectnode") == 0)
	{
		gridNav_DisconnectNode(pl);
		
		return 1;
	}
	
	// Deletes a node either on argument or closest to the client.
	if(Q_stricmp (cmd, "bot_gnav_delete") == 0 || Q_stricmp (cmd, "bot_gnav_deletenode") == 0)
	{
		OptionalSArgument = ConcatArgs( 1 );

		if (OptionalSArgument && OptionalSArgument[0]){
			gridNav_DeleteNode(pl, atoi(OptionalSArgument));
		}else{
			gridNav_DeleteNode(pl, -1);
		}
		
		return 1;
	}
	
	// Creates a trail for each passed node.
	if(Q_stricmp (cmd, "bot_gnav_trail") == 0 || Q_stricmp (cmd, "bot_gnav_createtrail") == 0){
		createTrail = !createTrail;
		
		trap_SendServerCommand(pl-g_entities, va("print \""
			BRAINWORKS_MSG "Trail create ^1%s^7.\n\"", (createTrail ? "enabled" : "disabled")));
		
		if(createTrail && gnavLastPrintedIndex != -1){
			trap_SendServerCommand(pl-g_entities, va("print \""
				BRAINWORKS_MSG "Using node %i as start node.\n\"", gnavLastPrintedIndex));
			
			gnavLastVisitedNode = gnavLastPrintedIndex;
		}else{
			gnavLastVisitedNode = -1;
		}
		
		return 1;
	}
}

/*
================================
gridNav_FindEmptyNode

Finds an empty node in the node
array.
================================
*/

int gridNav_FindEmptyNode()
{
	int i;
	
	for(i = 0; i < MAX_NODE_ARRAY_SIZE; i++){
		if(!gnavNodes[i] || !gnavNodes[i]->inuse){
			return i;
		}
	}
	
	return -1;
}

/*
================================
gridNav_CreateNewNode

This function is responsible for
creating a new node and placing
it in the level.
================================
*/

void gridNav_CreateNewNode(vec3_t origin, int flags)
{
	// First find an empty spot.
	int nodeNum = gridNav_FindEmptyNode();
	
	if (nodeNum == -1)
	{
		Com_Printf(S_COLOR_RED "ERROR: Could not find an empty node to use.\n");
		return;
	}

	if (!gnavNodes[nodeNum])
	{
		gnavNodes[nodeNum] = (gnavobject_t *)B_Alloc(sizeof(gnavobject_t));
	}

	if (!gnavNodes[nodeNum])
	{
		Com_Printf(S_COLOR_RED "ERROR: Could not allocate memory for node\n");
		return;
	}

	gnavNodes[nodeNum]->flags = flags;
	gnavNodes[nodeNum]->weight = 0; //calculated elsewhere
	gnavNodes[nodeNum]->index = nodeNum;
	gnavNodes[nodeNum]->inuse = 1;
	gnavNodes[nodeNum]->numNeighbors = 0; // Nothing (yet).
	VectorCopy(origin, gnavNodes[nodeNum]->origin);
	
	numNodes++;
}

/*
================================
gridNav_SelectNode

Selects a node based on where
the player currently is.
================================
*/

void gridNav_SelectNode(gentity_t *pl)
{
	if(gnavLastPrintedIndex == selectedNode && selectedNode != -1)
	{
		trap_SendServerCommand(pl-g_entities, va("print \""
			BRAINWORKS_MSG "Node %i is already selected!\n\"", selectedNode));
			
			return;
	}
	
	if(gnavLastPrintedIndex >= 0 && gnavLastPrintedIndex < MAX_NODE_ARRAY_SIZE && gnavNodes[gnavLastPrintedIndex]->inuse)
	{
		// Select the node.
		selectedNode = gnavLastPrintedIndex;
		
		trap_SendServerCommand(pl-g_entities, va("print \""
			BRAINWORKS_MSG "Selected node %i.\n\"", selectedNode));
	
		return;
	}
	
	// The node wasn't successfully selected.
	trap_SendServerCommand(pl-g_entities, "print \""
			BRAINWORKS_MSG "Unable to select: you're not close to a node!\n\"");
}

/*
================================
gridNav_ConnectNode

Connects two nodes to each other,
based on what node was selected
and where the player currently is.

Returns qfalse if the node wasn't
successfully connected.
================================
*/

qboolean gridNav_ConnectNode(gentity_t *pl, int start, int end)
{
	int node1, node2;
	int n = 0;
	
	// Perform some sanity checks.
	if(start == -1 && end == -1)
	{
		// These sanity checks are for when we're connecting two nodes based on manual selection and connection.
		if(selectedNode == -1)
		{
			trap_SendServerCommand(pl-g_entities, "print \""
				BRAINWORKS_MSG "Error: no node selected.\n\"");
		
			return qfalse;
		}
	
		if(gnavNodes[selectedNode]->numNeighbors == MAX_NODE_NEIGHBORS)
		{
			trap_SendServerCommand(pl-g_entities, va("print \""
				BRAINWORKS_MSG "Error: unable to add more neighbors to selected node (max: %i)!\n\"", MAX_NODE_NEIGHBORS));
				
			return qfalse;
		}
		
		if(gnavLastPrintedIndex == -1)
		{
			// Not close to a node.
			trap_SendServerCommand(pl-g_entities, "print \""
				BRAINWORKS_MSG "Error: unable to connect - not close to a node!\n\"");
		
			return qfalse;
		}
		
		if(selectedNode == gnavLastPrintedIndex)
		{
			trap_SendServerCommand(pl-g_entities, "print \""
				BRAINWORKS_MSG "Error: the node cannot connect to itself!\n\"");
		
			return qfalse;
		}
		
		// And verify for the destination node that we didn't hit the limit yet.
		if(gnavNodes[gnavLastPrintedIndex]->numNeighbors == MAX_NODE_NEIGHBORS)
		{
			trap_SendServerCommand(pl-g_entities, va("print \""
				BRAINWORKS_MSG "Error: unable to add more neighbors to destination node (max: %i)!\n\"", MAX_NODE_NEIGHBORS));
				
			return qfalse;
		}
		
		node1 = selectedNode;
		node2 = gnavLastPrintedIndex;
	}
	else
	{
		// These sanity checks are for when connecting nodes via a trail (automatically).
		// The return value here is important to avoid duplicate (failed) messages to the client.
		// If nodes fail to connect, the gridnav renderer will automatically disable the trail creation.
		if(gnavNodes[start]->numNeighbors == MAX_NODE_NEIGHBORS)
		{
			trap_SendServerCommand(pl-g_entities, va("print \""
				BRAINWORKS_MSG "Error: unable to add more neighbors to the previous node (max: %i)!\n\"", MAX_NODE_NEIGHBORS));
				
			return qfalse;
		}
		
		if(gnavNodes[end]->numNeighbors == MAX_NODE_NEIGHBORS)
		{
			trap_SendServerCommand(pl-g_entities, va("print \""
				BRAINWORKS_MSG "Error: unable to add more neighbors to the current node (max: %i)!\n\"", MAX_NODE_NEIGHBORS));
				
			return qfalse;
		}
		
		// Also check if the nodes are already connected (to avoid duplicate connections).
		while(n < gnavNodes[start]->numNeighbors){
			if(gnavNodes[start]->neighborIds[n] == end){
				trap_SendServerCommand(pl-g_entities, va("print \""
				BRAINWORKS_MSG "The previous node %i is already connected to the current node %i.\n\"", start, end));
			
				return qfalse;
			}
			n++;
		}
		
		node1 = start;
		node2 = end;
	}
	
	// Connect the two nodes.
	gnavNodes[node1]->neighborIds[gnavNodes[node1]->numNeighbors++] = node2;
	gnavNodes[node2]->neighborIds[gnavNodes[node2]->numNeighbors++] = node1;
	
	trap_SendServerCommand(pl-g_entities, va("print \""
			BRAINWORKS_MSG "Connected node %i with %i.\n\"", node1, node2));
			
	return qtrue;
}

/*
================================
gridNav_DisconnectNode

Disconnects two nodes from each
other, based on what node was 
selected and where the player
currently is.
================================
*/

void gridNav_DisconnectNode(gentity_t *pl)
{
	int 		i, x;
	int 		nodeNums[2];
	qboolean	order;
	
	// Perform some sanity checks.
	if(selectedNode == -1)
	{
		trap_SendServerCommand(pl-g_entities, "print \""
			BRAINWORKS_MSG "Error: no node selected.\n\"");
	
		return;
	}
	
	if(gnavLastPrintedIndex == -1)
	{
		// Not close to a node.
		trap_SendServerCommand(pl-g_entities, "print \""
			BRAINWORKS_MSG "Error: unable to disconnect - not close to a node!\n\"");
	
		return;
	}
	
	if(selectedNode == gnavLastPrintedIndex)
	{
		trap_SendServerCommand(pl-g_entities, "print \""
			BRAINWORKS_MSG "Error: the node cannot disconnect itself!\n\"");
	
		return;
	}
	
	// Disconnect the node by removing its relation (both ways).
	nodeNums[0] = selectedNode;
	nodeNums[1] = gnavLastPrintedIndex;
	
	for(i = 0; i < 2; i++){
		// Most of the time nodes only have one neighbor (or the node to be deleted was added last).
		// If this is the case, we don't need to do anything except remove the last neighbor in the list.
		// If the node is somewhere in the middle, we need to re-order this list.
		if(gnavNodes[nodeNums[i]]->neighborIds[gnavNodes[nodeNums[i]]->numNeighbors-1] != (i == 0 ? nodeNums[1] : nodeNums[0])){
			// Re-order the connections in the existing node while deleting the connection to the node that's about to be deleted.
			x = -1;
			order = qfalse;
			while(x < gnavNodes[nodeNums[i]]->numNeighbors-1){
				if(!order){
					if(gnavNodes[nodeNums[i]]->neighborIds[x+1] == (i == 0 ? nodeNums[1] : nodeNums[0])){
						// Found the entry. Start moving the next iteration.
						order = qtrue;
					}
				}else{
					gnavNodes[nodeNums[i]]->neighborIds[x] = gnavNodes[nodeNums[i]]->neighborIds[x+1];
				}
				
				x++;
			}
		}
		
		gnavNodes[nodeNums[i]]->neighborIds[gnavNodes[nodeNums[i]]->numNeighbors] = -1;
		gnavNodes[nodeNums[i]]->numNeighbors--; // One less neighbor.
	}
	
	trap_SendServerCommand(pl-g_entities, va("print \""
			BRAINWORKS_MSG "Disconnected node %i from %i.\n\"", nodeNums[0], nodeNums[1]));
}

/*
================================
gridNav_DeleteNode

Deletes a node based on:
 - The argument specified, or
 - where the client currently is.
================================
*/

void gridNav_DeleteNode(gentity_t *pl, int index){
	int x;
	int n = 0;
	int nodeNum, deleteNum;
	qboolean order;
	
	if(index > -1){
		// Node number specified.
		if(index > MAX_NODE_ARRAY_SIZE || !gnavNodes[index]->inuse)
		{
			trap_SendServerCommand(pl-g_entities, va("print \""
				BRAINWORKS_MSG "Error: Node %i is not in the level.\n\"", index));
			
			return;
		}
		
		deleteNum = index;
	}else{
		// Check where the player is.
		if(gnavLastPrintedIndex == -1)
		{
			// Not close to a node.
			trap_SendServerCommand(pl-g_entities, "print \""
				BRAINWORKS_MSG "Error: unable to delete - not close to a node!\n\"");
		
			return;
		}
		
		if(gnavLastPrintedIndex == selectedNode)
		{
			// Deselect the currently selected node.
			selectedNode = -1;
		}
		
		deleteNum = gnavLastPrintedIndex;
	}
		
	// Remove the node and its neighbor connections (since it's present).
	while(n < gnavNodes[deleteNum]->numNeighbors){
		nodeNum = gnavNodes[deleteNum]->neighborIds[n];
		
		// Most of the time nodes only have one neighbor (or the node to be deleted was added last).
		// If this is the case, we don't need to do anything except remove the last neighbor in the list.
		// If the node is somewhere in the middle, we need to re-order this list.
		if(gnavNodes[nodeNum]->neighborIds[gnavNodes[nodeNum]->numNeighbors-1] != deleteNum){
			// Re-order the connections in the existing node while deleting the connection to the node that's about to be deleted.
			x = -1;
			order = qfalse;
			while(x < gnavNodes[nodeNum]->numNeighbors-1){
				if(!order){
					if(gnavNodes[nodeNum]->neighborIds[x+1] == deleteNum){
						// Found the entry. Start moving the next iteration.
						order = qtrue;
					}
				}else{
					gnavNodes[nodeNum]->neighborIds[x] = gnavNodes[nodeNum]->neighborIds[x+1];
				}
				
				x++;
			}
		}
		
		gnavNodes[nodeNum]->neighborIds[gnavNodes[nodeNum]->numNeighbors] = -1;
		gnavNodes[nodeNum]->numNeighbors--; // One less neighbor.
		n++;
	}
	
	// Delete the node.
	gnavNodes[deleteNum]->inuse = qfalse;
	gnavNodes[deleteNum]->index = -1;
	gnavNodes[deleteNum]->numNeighbors = -1;
	gnavNodes[deleteNum]->neighborIds[0] = -1;
	
	trap_SendServerCommand(pl-g_entities, va("print \""
			BRAINWORKS_MSG "Deleted node %i.\n\"", deleteNum));
	
	numNodes--;
}

/*
================================
gridNav_RenderNodes

This function renders all nodes
to the in-game clients, so they
can edit the nodes and routes
easily.
================================
*/

void gridNav_RenderNodes(void)
{
	int i, n, count;
	int inc_checker;
	int bestindex;
	int gotbestindex;
	float bestdist;
	float checkdist;
	gentity_t *plum;
	gentity_t *viewent;
	char *flagstr;
	vec3_t a;
	char neighborStr[128];

	if (!gBotEdit)
	{
		return;
	}

	bestindex = 0;

	if (gnavRenderTime > level.time)
	{
		goto checkprint;
	}

	gnavRenderTime = level.time + 100;

	i = gnavRenderedFrame;
	inc_checker = gnavRenderedFrame;

	while(i < MAX_NODE_ARRAY_SIZE)
	{
		if (gnavNodes[i] && gnavNodes[i]->inuse)
		{
			// A "X" in-game represents a node (connected or not).
			// A line between two nodes represents the connection between two nodes.
			gridNav_DrawNode(gnavNodes[i]->origin, i);

			// Visualize the connection between nodes.
			n = 0;
			while (n < gnavNodes[i]->numNeighbors)
			{
				// Only draw connections to nodes that have a bigger index (to avoid duplicate lines and thus entities).
				if(gnavNodes[gnavNodes[i]->neighborIds[n]]->index > i){
					gridNav_DrawLine(gnavNodes[i]->origin, gnavNodes[gnavNodes[i]->neighborIds[n]]->origin, i);
				}
				n++;
			}
			
			gnavRenderedFrame++;
		}
		
		if(i == numNodes){
			// Looped through all alive nodes.
			gnavRenderedFrame = 0;
			break;
		}

		if ((i - inc_checker) > 4)
		{
			break; //don't render too many at once
		}
		
		i++;
	}

	if (i >= numNodes)
	{
		gnavRenderTime = level.time + 1500; //wait a bit after we finish doing the whole trail
		gnavRenderedFrame = 0;
	}

checkprint:

	if (!bot_wp_info.value)
	{
		return;
	}

	viewent = &g_entities[0]; //only show info to the first client

	if (!viewent || !viewent->client)
	{ //client isn't in the game yet?
		return;
	}

	bestdist = 256; //max distance for showing point info
	gotbestindex = 0;

	i = 0;
	count = 0;

	for(i = 0; i < MAX_NODE_ARRAY_SIZE; i++)
	{
		if (!gnavNodes[i] || !gnavNodes[i]->inuse){
			continue;
		}
		
		VectorSubtract(viewent->client->ps.origin, gnavNodes[i]->origin, a);

		checkdist = VectorLength(a);

		if (checkdist < bestdist)
		{
			bestdist = checkdist;
			bestindex = i;
			gotbestindex = 1;
		}

		count++;
		if(count == numNodes){
			break;
		}
	}

	if (gotbestindex && bestindex != gnavLastPrintedIndex)
	{
		gnavLastPrintedIndex = bestindex;
		
		if(createTrail){
			if(gnavLastVisitedNode == -1){
				gnavLastVisitedNode = bestindex;
			}else if(gnavLastVisitedNode != bestindex){
				// Not connected yet, connect them with each other.
				if(!gridNav_ConnectNode(viewent, gnavLastVisitedNode, bestindex)){
					// The nodes didn't connect successfully..
					trap_SendServerCommand(viewent-g_entities, "print \""
						BRAINWORKS_MSG "Trail create ^1disabled ^7due to error.\n\"");
					
					createTrail = qfalse;
					gnavLastVisitedNode = -1;
				}else{
					// Success!
					gnavLastVisitedNode = bestindex;
				}
			}
		}else{
			// Append neighbors to a char buffer so we can use it in the info message.
			neighborStr[0] = '\0';
			if(gnavNodes[bestindex]->numNeighbors){
				if(gnavNodes[bestindex]->numNeighbors == 1){
					Q_strcat(neighborStr, sizeof(neighborStr), va("[%i]", gnavNodes[bestindex]->neighborIds[0]));
				}else{
					for(i = 0; i < gnavNodes[bestindex]->numNeighbors; i++){
						if(i == 0){
							Q_strcat(neighborStr, sizeof(neighborStr), "[");
						}
						
						if((i +1) != gnavNodes[bestindex]->numNeighbors){
							Q_strcat(neighborStr, sizeof(neighborStr), va("%i, ", gnavNodes[bestindex]->neighborIds[i]));
						}else{
							// Last in the array.
							Q_strcat(neighborStr, sizeof(neighborStr), va("%i]\0", gnavNodes[bestindex]->neighborIds[i]));
						}
					}
				}
			}
			
			trap_SendServerCommand(viewent-g_entities, va("print \""
				BRAINWORKS_MSG "Node %i\nOrigin: (%i %i %i)\nNeighbors: %i %s\n\"",
				(int)(gnavNodes[bestindex]->index), (int)(gnavNodes[bestindex]->origin[0]), (int)(gnavNodes[bestindex]->origin[1]), (int)(gnavNodes[bestindex]->origin[2]), (int)(gnavNodes[bestindex]->numNeighbors), neighborStr
				));
		}
	}
	else if (!gotbestindex)
	{
		gnavLastPrintedIndex = -1;
	}
}

/*
================================
gridNav_DrawLine

This function draws a line in-game.
================================
*/

void gridNav_DrawLine(vec3_t start, vec3_t end, int i)
{
	gentity_t *temp;
	
	temp = G_TempEntity( start, EV_BOTWAYPOINT );
	VectorCopy(end, temp->s.angles);
	temp->r.svFlags |= SVF_BROADCAST;
	temp->s.time = i;
}

/*
================================
gridNav_DrawNode

This function draws a node in-game.
Nodes are represented as a "X",
which are just two crossed lines.
================================
*/

void gridNav_DrawNode(vec3_t centerOrigin, int i)
{
	vec3_t origin0, origin1;
	
	// Set the bounds on the 1st line.
	VectorCopy(centerOrigin, origin0);
	origin0[0] += NODE_BOUND_SIZE;
	origin0[1] += NODE_BOUND_SIZE;
	origin0[2] += NODE_BOUND_SIZE;
	
	VectorCopy(centerOrigin, origin1);
	origin1[0] -= NODE_BOUND_SIZE;
	origin1[1] -= NODE_BOUND_SIZE;
	origin1[2] -= NODE_BOUND_SIZE;
	
	// Display it.
	gridNav_DrawLine(origin0, origin1, i);
	
	// Set the bounds on the 2nd line.
	VectorCopy(centerOrigin, origin0);
	origin0[0] -= NODE_BOUND_SIZE;
	origin0[1] -= NODE_BOUND_SIZE;
	origin0[2] += NODE_BOUND_SIZE;
	
	VectorCopy(centerOrigin, origin1);
	origin1[0] += NODE_BOUND_SIZE;
	origin1[1] += NODE_BOUND_SIZE;
	origin1[2] -= NODE_BOUND_SIZE;
	
	// Display it.
	gridNav_DrawLine(origin0, origin1, i);
}
